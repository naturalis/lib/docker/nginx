#!/bin/sh
set -eu

latest_image=$CI_REGISTRY_IMAGE:latest
basedir=$(dirname "$0")

if docker pull "$latest_image"; then
  if ! docker run --rm --user root "$latest_image" sh -c \
    'if [ -d /var/cache/apk ]; then
      if ! apk list --no-cache --upgradable | grep upgradable; then
        exit 0
      fi
    elif [ -d /var/cache/apt/ ]; then
      if apt-get update && apt-get upgrade --simulate | grep -e "^0 upgraded"; then
        exit 0
      fi
    fi
    exit 1'
  then
    docker image rm "$latest_image"
  fi
fi

docker build --pull --cache-from "$latest_image" \
  --tag "$CI_REGISTRY_IMAGE:build_$CI_COMMIT_REF_SLUG" "$basedir"
docker push "$CI_REGISTRY_IMAGE:build_$CI_COMMIT_REF_SLUG"
