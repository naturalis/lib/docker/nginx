FROM nginx:alpine

WORKDIR /var/www

# hadolint ignore=DL3017
RUN apk update && apk upgrade --no-cache
